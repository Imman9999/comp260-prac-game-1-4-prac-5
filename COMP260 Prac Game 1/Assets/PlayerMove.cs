﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour {

	private BeeSpawner beeSpawner;

	public Vector2 velocity; // in metres per second
	public float maxSpeed = 5.0f;

	public float destroyRadius = 1.0f; 

	// Use this for initialization
	void Start () {
		beeSpawner = FindObjectOfType<BeeSpawner> ();

	}
	
	// Update is called once per frame
	void Update () {
		// get the input values
		Vector2 direction;
		direction.x = Input.GetAxis("Horizontal");
		direction.y = Input.GetAxis("Vertical");

		// scale by the maxSpeed parameter
		Vector2 velocity = direction * maxSpeed;

		// move the object
		transform.Translate(velocity * Time.deltaTime);


		if (Input.GetButton ("Fire1")) {
			// destroy nearby bees
			beeSpawner.DestroyBees(
				transform.position, destroyRadius);
		}

	}
}
