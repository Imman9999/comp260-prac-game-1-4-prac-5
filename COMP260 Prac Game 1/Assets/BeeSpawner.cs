﻿using UnityEngine;
using System.Collections;

public class BeeSpawner : MonoBehaviour {

	public BeeMove beePrefab;
	public int nBees = 50;

	public float xMin, yMin;
	public float width, height;

	public float minSpawnTime = 0.5f;
	public float maxSpawnTime = 5.0f;

	public float randomTime;


	// Use this for initialization
	void Start () {
		randomTime = Random.Range (minSpawnTime, maxSpawnTime);
	}
	
	// Update is called once per frame
	void Update () {
		randomTime -= Time.deltaTime;

		if (randomTime <= 0.0f) {
			BeeMove bee = Instantiate (beePrefab);          

			float x = xMin + Random.value * width;
			float y = yMin + Random.value * height;
			bee.transform.position = new Vector2 (x, y);


			PlayerMove player = 
				(PlayerMove)FindObjectOfType (typeof(PlayerMove));
			beePrefab.target = player.transform;

			randomTime = Random.Range (minSpawnTime, maxSpawnTime);
		}
	}



	public void DestroyBees(Vector2 centre, float radius) {
		// destroy all bees within ‘radius’ of ‘centre’
		for (int i = 0; i < transform.childCount; i++) {
			Transform child = transform.GetChild(i);
			// BUG! the line below doesn’t work
			Vector2 v = (Vector2)child.position - centre;
			if (v.magnitude <= radius) {
				Destroy(child.gameObject);
			}
		}
	}



}
